# Library for basic bit operations

## Description
 Contains macros, which can simplify code. For example macro set_bit:
  set_bit(variable_i, 2);  // Set second bit (counted from LSB) in variable
 
## Files
 * bit_operations.c - functions
 * bit_operations.h - macros

## Usage
 Just include bit_operations.h and compile bit_operations.c file.
 
## Notes
 * I/O operations are only for AVR8 architecture!